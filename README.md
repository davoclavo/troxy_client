# TroxyClient

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add troxy_client to your list of dependencies in `mix.exs`:

        def deps do
          [{:troxy_client, "~> 0.0.1"}]
        end

  2. Ensure troxy_client is started before your application:

        def application do
          [applications: [:troxy_client]]
        end

